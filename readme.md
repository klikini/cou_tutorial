# Children of Ur Tutorial

## About

This tutorial, part of the [Children of Ur project](https://github.com/ChildrenOfUr), is designed for new players, as well as players of the original game, [Glitch](http://glitchthegame.com) who need a refresher.

## Technical Stuff

The tutorial site is written in plain HTML5 that is styled with CSS3. A very tiny bit of PHP is used to pick a random background image for the introduction section. All of the files that get uploaded to the server are stored in the _web_ directory. Assets (images like the screenshots, backgrounds, favicon/logo, etc) are stored under _web/resources_.

Each time a commit is made and merged, I will upload the contents of the _web_ directory to the site's server.

## Contributing

You can contribute by clarifying any information, or adding new content.

The tutorial only works correctly when hosted on an Apache web server, but you can get most of the page to render by downloading a .zip file of the master branch.
You will need to rename the .php extensions to .html (unless you are running an [XAMPP](https://www.apachefriends.org/index.html) server on your computer), and linking back to the main page won't work (.htaccess requires Apache).
Open tutorial.php (or .html) to view the site.

### What Needs to be Done

1. Each section has a different background/text colour. Some are hard to read with different monitor settings, so please change any colours you have trouble seeing. The colours are defined at the bottom of coututorial.css, grouped by section ids, which can be found in the tutorial pages.
2. The page on how to play (resources, gardening, cooking, buying/selling, etc.) currently has the same content as the screen page as a placeholder. This needs to be filled in. Add callouts to screenshots (they should go outside the image area onto a transparent area) from the _web/resources/callouts/_ folder. See [this image](https://github.com/Klikini/cou-tutorial/blob/master/calloutexample.png) for an idea of how they will look. If you need to use custom text, the font is [Varela Round](https://github.com/Klikini/cou-tutorial/blob/master/VarelaRound.ttf?raw=true).

### More Info
If you need help, have questions, or want to tell me something, contact me on my [website](http://robiotic.net/contact).