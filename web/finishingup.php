<!doctype html>
<html>
  <head>
  	<?php include("headtag.php"); ?>
    <title>CoU Tutorial - Finishing Up</title>
  </head>
  <body>
    <main>
      <div id="header">
          <div>
              <a class="noa" id="title" href="#"><img src="resources/logo.svg"><span id="tutorialtitle">Tutorial</span></a>
              <ul id="progress">
                  <li class="progress-back"><a href="tutorial">Start</a></li>
                  <li class="progress-back"><a href="screenelements">Screen</a></li>
                  <li class="progress-back"><a href="playing">Playing</a></li>
                  <li class="progress-back"><a href="practice">Practice</a></li>
                  <li class="progress-current">Finishing Up</li>
              </ul>
              <div id="progress-current">Finishing Up</div>
          </div>
      </div>
      <div id="content">
          <section id="rulesandguidelines">
              <h1>Rules & Guidelines</h1>
              <h2><i class="fa fa-eraser faicon"></i> Language</h2>
              <p>You will not be blocked for using some of the more harmless curse words, however, insulting other players may get you blocked if it is reported or done publicly.</p>
              <h2><i class="fa fa-times faicon"></i> Cheating</h2>
              <p>You are allowed to create multiple accounts, but do not use them to help your main account. Use them separately. You will be blocked if multiple accounts are discovered helping another account with the same owner.</p>
              <p>You will be banned if you are caught modifying locally cached code to help yourself in the game (eg. no gravity, editing wealth, etc.)</p>
              <h2><i class="fa fa-crosshairs faicon"></i> Reporting Bugs</h2>
              <p>If you have discovered a bug, click the bug report icon (it's a bug) at the top right of your screen. Describe your problem, and we'll see what we can do to fix it.</p>
              <h2><i class="fa fa-users faicon"></i> Contributing</h2>
              <p>If you want to contribute to the game effort, you can! Check out our <a class="lighta" href="https://github.com/ChildrenOfUr/coUclient" target="_blank">Github page</a> for more information and to see what you can do to help.</p>
              <h2><i class="fa fa-cubes faicon"></i> Visit Us</h2>
              <p>Check us out on <a class="lighta" href="https://plus.google.com/u/0/communities/103304995470607753666" target="_blank">Google+</a>, <a class="lighta" href="https://www.facebook.com/childrenofur" target="_blank">Facebook</a>, and <a class="lighta" href="https://twitter.com/childrenofur" target="_blank">Twitter</a> as well.</p>
          </section>
          <section id="finishingup">
              <h1>Finishing Up</h1>
              <span class="ct">
                  <h2>Tutorial completed. You did great!</h2>
                  <h3>Now that you know how to play...</h3>
                  <center>
                    <a href="http://childrenofur.github.io/coUclient/game.html?ref=tutorial">
                      <button class="button btn-huge btnbounce bounce-right btn-fancy">Play Now&emsp;<i class="fa fa-arrow-right"></i></button>
                    </a>
                  </center>
              </span>
          </section>
  		<section id="footer">
  			<p class="ct">For more help, ask in Live Help inside the game chat.</p>
        <p class="ct">If you need to review any part of the tutorial, click a section at the top of the page.</p>
        <a title="Tutorial by Klikini" id="credit"><i id="credit" class="fa fa-info-circle"></i></a>
  		</section>
      </div>
    </main>
  </body>
</html>