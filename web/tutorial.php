<!doctype html>
<html>
  <?php
    $listBg = scandir ('resources/backgrounds/web');
  	$numBg = rand(0, count($listBg)-1);
  	$selectedBg = "$listBg[$numBg]";
    $bgPath = 'resources/backgrounds/web/' . $selectedBg;
  ?>
<head>
  	<?php include("headtag.php"); ?>
	<style type="text/css">
		<!--
			#start {
        background: linear-gradient(rgba(75, 46, 76, 0.2),rgba(75, 46, 76, 0.2)), url(<?php echo $bgPath; ?>);
        background-position: top center;
			}
		-->
	</style>
  <title>CoU Tutorial</title>
</head>
<body>
  <main>
    <div id="header">
        <div>
            <a class="noa" id="title" href="#"><img src="resources/logo.svg"><span id="tutorialtitle">Tutorial</span></a>
            <ul id="progress">
            	<li class="progress-current">Start</li>
            	<li class="progress-next">Screen</li>
            	<li class="progress-next">Playing</li>
            	<li class="progress-next">Practice</li>
            	<li class="progress-next">Finishing Up</li>
            </ul>
            <div id="progress-current">Start</div>
        </div>
    </div>
    <div id="content">
        <section id="start" class="ct">
            <h1>Welcome to the Children of Ur tutorial!</h1>
            <p>Children of Ur is a remake of the now-defunct <a class="lighta" href="http://en.wikipedia.org/wiki/MMORPG" target="_blank" title="Massively Multiplayer Online Role-Playing Game">MMORPG</a>, <a class="lighta" href="http://glitchthegame.com" target="_blank">Glitch</a>.</p>
						<p>Here, you will learn how to play, and a few things we expect of players.</p>
						<p>
              <a href="screenelements"><button class="button btnbounce bounce-right">Get Started&emsp;<i class="fa fa-arrow-right"></i></button></a>
           	</p>
        </section>
        <a id="ghlink" href="https://github.com/Klikini/cou-tutorial" target="_blank" alt="Github"><div id="ghlink"></div></a>
    </div>
  </main>
</body>
</html>