<!-- TODO: change max-width of media query in coututorial.css on line 502 when page launches -->
<!doctype html>
<html>
	<head>
	  <?php include("headtag.php"); ?>
	  <title>CoU Tutorial - Practice</title>
	</head>
	<body>
	  <main>
	    <div id="header">
	        <div>
	            <a class="noa" id="title" href="#"><img src="resources/logo.svg"><span id="tutorialtitle">Tutorial</span></a>
	            <ul id="progress">
	                <li class="progress-back"><a href="tutorial">Start</a></li>
	                <li class="progress-back"><a href="screenelements">Screen</a></li>
	                <li class="progress-back"><a href="playing">Playing</a></li>
	                <li class="progress-current">Practice</li>
	                <li class="progress-next">Finishing Up</li>
	            </ul>
	            <div id="progress-current">Practice</div>
	        </div>
	    </div>
	    <div id="content">
	    	<section id="practice">
	    		<h1>Practice</h1>
	    		<p>On this page, you'll get to practice some of the things you've learned so far.</p>
	    		<p>Follow the instructions under each game window.</p>
	    	</section>
	        <section id="pr-chat">
	            <h1>Sending and Receiving Chat Messages</h1>
	            <div class="practicewindow" id="pr-chat-w"></div>
	            <p>Reply to the message.</p>
	        </section>
	        <section id="pr-navi">
	            <h1>Navigating</h1>
	            <div class="practicewindow" id="pr-navi-w"></div>
	            <p>Go to the street called <em>Groddle Forest Junction</em>.</p>
	        </section>
	        <section id="pr-planttree">
	            <h1>Planting a Tree</h1>
	            <div class="practicewindow" id="pr-planttree-w"></div>
	            <p>Season the bean and plant the tree.</p>
	        </section>
	        <section id="finishingup">          
                <h1>Moving On...</h1>
                <span class="ct">
                  <p>You're almost done!</p>
                  <center>
                    <a href="finishingup">
                      <button class="button btnbounce bounce-right">Finishing Up&emsp;<i class="fa fa-arrow-right"></i></button>
                    </a>
                  </center>
                </span>
            </section>
	        <section id="footer">
	          <p class="ct">Don't worry, you'll get plenty of practice in the game!</p>
	        </section>
	    </div>
	  </main>
	</body>
</html>