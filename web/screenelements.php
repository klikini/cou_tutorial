<!doctype html>
<html>
    <head>
      <?php include("headtag.php"); ?>
      <title>CoU Tutorial - Screen</title>
    </head>
    <body>
      <main>
        <div id="header">
            <div>
                <a class="noa" id="title" href="#"><img src="resources/logo.svg"><span id="tutorialtitle">Tutorial</span></a>
                <ul id="progress">
                    <li class="progress-back"><a href="tutorial">Start</a></li>
                    <li class="progress-current">Screen</li>
                    <li class="progress-next">Playing</li>
                    <li class="progress-next">Practice</li>
                    <li class="progress-next">Finishing Up</li>
                </ul>
                <div id="progress-current">Screen</div>
            </div>
        </div>
        <div id="content">
            <section id="screenelements">
                <h1>Screen Elements</h1>
                <img class="screenshot" src="resources/screenshots/screenelements_screenshot.png" />
                <p>The screen interface is organized very much like Glitch. It is divided into several sections, and each section has a purpose in the game.</p>
                <p>If you played Glitch, the interface is similar enough that you will know how to play. Read the tutorial to refresh your memory, or...</p>
                <p>
                  <br />
                  <a href="#finishingup"><center><button class="button btnbounce bounce-down">Skip to the End&emsp;<i class="fa fa-arrow-down"></i></button></a></center>
                </p>
            </section>
            <section id="playerstatus">
                <h1>Player Status</h1>
                <img src="resources/screenshots/screenelements_playerstatus.png" class="screenshot" />
                <p>The player status area shows your player's mood, energy, and imagination.</p>
                <h2><i class="fa fa-smile-o faicon"></i> Mood</h2>
                <p>Your player's mood is important to gameplay, but not crucial to player survival. It controls how much imagination you can earn for your activities. You can gain mood through quests, quoins, achievements, drinking things, etc.</p>
                <p>Your mood is displayed in the circle, with your avatar's face. The color of the circle and your avatar's facial expression will show your mood.</p>
                <h2><i class="fa fa-signal faicon"></i> Energy</h2>
                <p>Your player's energy is similar to health bars in other games. If it gets too low, you could die. Keep it up by eating food. Energy is lost more quickly when performing game activities, and is slowly sapped over time.</p>
                <h2><i class="fa fa-info-circle faicon"></i> Imagination</h2>
                <p>Imagination points (or iMG) can be used to buy character upgrades. Collecting certain amounts of iMG allows you to level up.</p>
            </section>
            <section id="imgmenu">
                <h1>iMG Menu</h1>
                <img class="screenshot" src="resources/screenshots/screenelements_imgmenu.png" />
                <p>The iMG menu allows you to spend your Imagination points.</p>
                <h2><i class="fa fa-book faicon"></i> Skills</h2>
                <p>Choose new skills to learn and view their status.</p>
                <h2><i class="fa fa-compass faicon"></i> Quests</h2>
                <p>View the quests you have available and their instructions.</p>
                <h2><i class="fa fa-level-up faicon"></i> Upgrade Cards</h2>
                <p>Buy cards with your iMG to unlock new abilities.</p>
            </section>
            <section id="property">
                <h1>Property</h1>
                <table class="multiscreenshot">
                    <tr>
                        <td>
                            <img class="screenshotofmany" src="resources/screenshots/screenelements_property_inventorymenu.png" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img class="screenshotofmany" src="resources/screenshots/screenelements_property.png" />
                        </td>
                    </tr>
                </table>
                <p>The property area of the screen has your currants, inventory, and keys to your house.</p>
                <h2><i class="fa fa-money faicon"></i> Currants</h2>
                <p>Currants are in-game currency. They are used to buy items, and you can earn them by selling items. You can give currants to other players, and sometimes they are given by achievements.</p>
                <h2><i class="fa fa-archive faicon"></i> Inventory</h2>
                <p>Your inventory holds the items you own. You can also drop items on the ground, in your house, etc. Your inventory has two areas: a quick-access bar at the bottom, and clicking the magnifying glass will open a menu with more slots. A slot can hold one kind of item. Some items can stack, so that more than one can fit in a stack. The maximum number of items in a stack varies based on the item.</p>
                <p>You can also purchase bags of several sizes to put in your inventory. A bag takes up only one inventory slot, but it can hold many more inside it.</p>
                <h2><i class="fa fa-key faicon"></i> Keys</h2>
                <p>A keyring icon will appear near the currants indicator when another player has given you keys to their house. Click here to open a list of houses you have access to, as well as a list of players you have allowed to access your house.</p>
                <p>A key is not needed to enter someone else's house. If the owner of the house is online, they will receive a notification here that another player is knocking at their door, and they can choose whether to let them in.</p>
            </section>
            <section id="chat">
                <h1>Chat</h1>
                <img class="screenshot" src="resources/screenshots/screenelements_chat.png" />
                <p>The chat area of the game allows you to communicate with players in the same street using local chat, your friends (regardless of location) using individual chat, groups of people using party chat, or anyone on global chat.</p>
                <h2><i class="fa fa-list-alt faicon"></i> Chat Menu</h2>
                <p>Open the chat menu by hovering over the arrow.</p>
                <h2><i class="fa fa-comment faicon"></i> Local Chat</h2>
                <p>Local chat allows you to communicate only with people in the same street. It automatically changes channels when you change streets.</p>
                <h2><i class="fa fa-comments faicon"></i> Global Chat</h2>
                <p>Global chat is one huge chat room. Anyone in the game can open it and communicate.</p>
                <h2><i class="fa fa-comment-o faicon"></i> Individual Chat</h2>
                <p>You can have a private conversation with one other player by clicking a friend's name in the chat menu.</p>
                <h2><i class="fa fa-comments-o faicon"></i> Party Chat</h2>
                <p>You can start party chats by selecting friends to include. Any of the people in the chat can invite more players. The chat is automatically closed when all players leave.</p>
                <h2><i class="fa fa-terminal faicon"></i> Chat Box</h2>
                <p>To send a chat message, type it into the chat box and press enter.</p>
            </section>
            <section id="gameinfo">
                <h1>Game Info</h1>
                    <img class="screenshot" src="resources/screenshots/screenelements_gameinfo.png" />
                    <p>The game information area gives you access to the map, settings, and clock.</p>
            </section>
            <section id="mappingandlocations">
                <h1>Mapping & Locations</h1>
                <table class="multiscreenshot">
                    <tr>
                        <td>
                            <img class="screenshotofmany" src="resources/screenshots/screenelements_map.png" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img class="screenshotofmany" src="resources/screenshots/screenelements_location.png" />
                        </td>
                    </tr>
                </table>
                <p>The mapping area displays your current street name and gives you access to a world map.</p>
                <h2><i class="fa fa-road faicon"></i> Street Name</h2>
                <p>The name of the street you are on is shown here. This is useful for telling friends, "I'm on this street, come meet me!"</p>
                <h2><i class="fa fa-map-marker faicon"></i> Map Button</h2>
                <p>Click here to open the map. When GPS mode is turned on, the next turn will be shown here as well.</p>
                <h2><i class="fa fa-location-arrow faicon"></i> Map Menu</h2>
                <p>The map menu displays the world map, and allows you to view region and street names. Clicking a street will give you options to teleport (at an expense), or to get walking directions.</p>
                <p>Walking directions give you GPS-like instructions, guiding you to your destination street.</p>
            </section>
            <section id="clock">
                <h1>Clock</h1>
                <img class="screenshot" src="resources/screenshots/screenelements_gameinfo_clock.png" />
                <p>The clock shows the current in-game time and date.</p>
                <h2><i class="fa fa-calendar faicon"></i> Calendar System</h2>
                <p>The in-game calendar system works much differently than a regular calendar, allowing for shorter game days.</p>
                <p>Clicking on the clock will open a complete calendar.</p>
            </section>
            <section id="settings">
                <h1>Settings</h1>
                <table class="multiscreenshot">
                    <tr>
                        <td>
                            <img class="screenshotofmany" src="resources/screenshots/screenelements_gameinfo_settings.png" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img class="screenshotofmany" src="resources/screenshots/screenelements_gameinfo_icons.png" />
                        </td>
                    </tr>
                </table>
                <h2><i class="fa fa-bug faicon"></i> Bug Report</h2>
                <p>Click the bug icon to file a bug report, or leave any other helpful feedback.</p>
                <h2><i class="fa fa-home faicon"></i> Homepage</h2>
                <p>Click the house icon to go to the Children of Ur homepage.</p>
                <h2><i class="fa fa-volume-up faicon"></i> Volume</h2>
                <p>Adjust the volume by hovering over the speaker icon. Click it to mute.</p>
                <h2><i class="fa fa-cog faicon"></i> Settings</h2>
                <p>Click the gear icon to open the settings menu.</p>
            </section>
            <section id="controls">
                <h1>Controls</h1>
                <img class="screenshot ss-noeffect" src="resources/screenshots/controls.png"/>
                <p>The controls of the game are very simple.</p>
                <h2><i class="fa fa-arrows-h faicon"></i> Walking</h2>
                <p>To move your character, use the left and right arrow keys. Press space to jump, and jump three times in a row to jump higher. Use the up arrow to climb ladders.</p>
                <h2><i class="fa fa-arrows faicon"></i> Interacting</h2>
                <p>Most game menu interaction is done with the mouse. Some menus may let you select with the arrow keys.</p>
            </section>
            <section id="finishingup">
                <h1>Moving On...</h1>
                <span class="ct">
                  <p>Now that you know what things do, don't you want to learn how to use them?</p>
                  <center>
                    <a href="playing">
                      <button class="button btnbounce bounce-right">Playing the Game&emsp;<i class="fa fa-arrow-right"></i></button>
                    </a>
                  </center>
                </span>
            </section>
        </div>
      </main>
    </body>
</html>